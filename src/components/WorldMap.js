import Vue from 'vue';

import { data as MAP_DATA, geoToLocation } from './map';

const refID = 'heatmap-canvas';

export default Vue.component('world-map', {
  props: {
    items: {
      type: Array,
      required: true,
      // the value must be a two-dimensional array
      validator: (value) => !(value.length === 0 || value[0].length === 0),
    },
    width: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
    gradient: {
      type: Array,
      required: true,
    },
    hover: {
      type: Function,
      required: false,
    },
  },

  data: () => ({
    ctx: undefined,
    point: undefined,
    closestPoint: undefined,
    mousePoint: undefined,
    buckets: [],
    matrix: [[]],
    radius: 11,
    yCount: 0,
    xCount: 0,
    maxValue: 0,
    min: 0,
    loading: true,
    frames: 0,
  }),
  mounted() {
    // initialize the drawing context after mount
    this.ctx = this.$refs[refID].getContext('2d');

    // do an initial resize of the chart drawing area and register the
    // event listener for resize events
    this.onResize();
    window.addEventListener('resize', this.onResize);
  },

  beforeDestroy() {
    // remove the event listeners we created during creation / mount
    window.removeEventListener('resize', this.onResize);
  },

  methods: {
    colorFromValue(value) {
      const { maxValue, gradient } = this;

      if (value === undefined) {
        return gradient[0];
      }

      const colorIndex = Math.floor(gradient.length * ((value - 1) / maxValue));
      return gradient[colorIndex];
    },

    /**
     * Redraw the canvas
     * @private
     */
    onRedraw() {
      const {
        ctx,
        width,
        height,
        matrix,
        loading,
      } = this;

      // if the canvas is not defined, there was a rendering error.
      if (ctx === undefined) {
        return;
      }

      window.requestAnimationFrame(() => {
        // reset background
        ctx.clearRect(0, 0, width, height);

        // draw the basic grid of data, row by row
        matrix.forEach((_) => _.forEach((item) => {
          const { x, y, value } = item;

          // get the color value for the point
          let color = this.colorFromValue(value);

          if (loading) {
            const lightness = 50 * (1 + Math.sin((x + y + Date.now()) / 200));
            color = `hsl(216,38%,${lightness}%)`;
            this.drawPoint(x, y, color);
            return;
          }

          if (value === undefined) {
            // this.drawPoint(x, y, color);
            return;
          }

          this.drawPentagon(x, y, color, false, `${value}`);
        }));

        // if a point is hovered, highlight it
        if (this.point !== undefined) {
          const { x, y, value } = this.point;
          this.drawPentagon(x, y, '#ffffff', false, `${value}`);
        }

        // if we're still loading redraw animation
        if (loading) {
          this.onRedraw();
        }
      });
    },

    /**
     * Handles a mouse hover event.
     * @param offsetX {number}
     * @param offsetY {number}
     */
    onHover(event) {
      const { offsetX, offsetY } = event;

      // find indexes and draw the pentagon
      const { radius, matrix } = this;

      // find the point in the matrix
      let point;
      matrix.some((_) => _.some((item) => {
        const { x, y, value } = item;
        const distance = Math.sqrt((offsetX - x) ** 2 + (offsetY - y) ** 2);
        if (distance < radius && value !== undefined) {
          point = item;
          return true;
        }
        return false;
      }));

      if (point === undefined) {
        this.point = undefined;
        this.onRedraw();
        return;
      }

      // update the point (if it has changed)
      if (
        this.point === undefined
        || point.x !== this.point.x
        || point.y !== this.point.y
      ) {
        this.point = point;
        this.onRedraw();

        if (this.hover) {
          this.hover(point);
        }
      }
    },

    /**
     * @private
     */
    onResize() {
      // the size of the tiles depend on how many tiles fit in vertically and horizontally
      const yCount = MAP_DATA.area[2];
      const xCount = MAP_DATA.area[3];

      const xDiameter = xCount / this.width;
      const yDiameter = yCount / this.height;

      let dX = 0;
      let dY = 0;
      let diameter = 0;
      let radius = 0;

      // calculate diameter for the pentagons and the resulting radius
      if (xDiameter > yDiameter) {
        diameter = this.width / (xCount + 1);
        radius = diameter / 2;
        dX = diameter;
        dY = Math.round(Math.sqrt(3 * radius * radius));
      } else {
        diameter = this.height / (yCount + 1);
        radius = diameter / 2;
        dY = diameter;
        dX = Math.floor(Math.sqrt((diameter * diameter) / 3)) * 2;
      }

      // how many data points can fit into each row or column?
      this.xCount = xCount;
      this.yCount = yCount;

      // calculate the width needed for each row and for each column
      const tmpWidth = this.xCount * dX + dX / 2;
      const tmpHeight = (this.yCount - 1) * dY + diameter;
      const xOffset = (this.width - tmpWidth) / 2;
      const yOffset = (this.height - tmpHeight) / 2;

      // initialize matrix
      this.matrix = [...Array(this.yCount)].map((_, y) => [...Array(this.xCount)].map((_1, x) => {
        const mod = y % 2;

        const px = xOffset + radius + x * dX + mod * radius;
        const py = yOffset + radius + y * dY; // clientHeight -

        return {
          x: px,
          y: py,
          iX: x,
          iY: y,
          value: undefined,
          color: undefined,
        };
      }));

      // Build continent shit
      MAP_DATA.continents.forEach((continent) => {
        const { dots } = continent;

        dots.forEach((dot) => {
          const [y, x, count] = dot;

          for (let px = x; px < x + count; px += 1) {
            this.matrix[y][px].value = 0;
          }
        });
      });

      // convert input data into a matrix
      let maxValue = 0;
      this.items.forEach((item) => {
        const { location, value } = item;

        // find the position for this item:
        const [x, y] = geoToLocation(location);
        if (this.matrix[y][x].value === undefined) {
          this.matrix[y][x].value = 0;
        }
        this.matrix[y][x].value += value;

        if (this.matrix[y][x].value > maxValue) {
          maxValue = this.matrix[y][x].value;
        }
      });

      // ensure that the canvas has the right size.
      this.maxValue = maxValue;
      this.radius = radius;
      this.loading = false;
      this.onRedraw();
    },

    /**
     * Draws a pentagon at the specified location.
     * @param x {number} x Position on the graph
     * @param y {number} y Position on the graph
     * @param color {string} Color for the pentagon
     */
    drawPentagon(x, y, color = '#2e4066') {
      const { ctx, radius } = this;
      if (ctx === undefined) {
        return;
      }

      // draw pentagon
      const xd = Math.cos((30 / 180) * Math.PI) * radius;
      const yd = Math.sin((30 / 180) * Math.PI) * radius;

      ctx.fillStyle = color;
      ctx.lineWidth = 1;
      ctx.translate(x, y);
      ctx.beginPath();
      ctx.moveTo(0, -radius);
      ctx.lineTo(xd, -yd);
      ctx.lineTo(xd, yd);
      ctx.lineTo(0, radius);
      ctx.lineTo(-xd, +yd);
      ctx.lineTo(-xd, -yd);
      ctx.lineTo(0, -radius);
      ctx.setTransform(1, 0, 0, 1, 0, 0);
      ctx.fill();
    },

    /**
     * Draws a point at the specified location.
     * @param x {number} x Position on the graph
     * @param y {number} y Position on the graph
     * @param color {string} Color for the point
     */
    drawPoint(x, y, color = '#1d2b41') {
      const { ctx } = this;
      if (ctx === undefined) {
        return;
      }

      ctx.fillStyle = color;
      ctx.fillRect(x - 0.5, y - 0.5, 1, 1);
    },
  },

  /**
   * Component's render function
   * @param {import('vue').CreateElement} createElement
   */
  render(createElement) {
    const { width, height, onHover } = this;

    return createElement('canvas', {
      ref: refID,
      class: 'canvas',
      attrs: {
        width,
        height,
      },
      on: {
        mousemove: onHover,
        // mouseup: onClick,
      },
    });
  },
});
