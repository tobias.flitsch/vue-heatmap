import WorldMap from './components/WorldMap';

WorldMap.install = (Vue) => {
  Vue.component(WorldMap.name, WorldMap);
};

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(WorldMap);
}

export default WorldMap;
