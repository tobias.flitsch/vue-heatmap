# Vue TF Worldmap

A Vue JS Component for displaying data on a world map.

## Installation

Install via npm using `npm install vue-tf-worldmap`.

## Usage

This component is most useful when displaying values in a world map view. Depending
on the color gradient supplied, values will be colorized by their intensity.

Example usage:

```javascript
import WorldMap from 'vue-tf-worldmap';
Vue.use(WorldMap);
```

Template:

```
<template>
  <div id="app">
    <div class="content">
      <world-map :items="items" :gradient="gradient" :hover="onHover" :width="1800" :height="1200" />
    </div>
  </div>
</template>
```

[Live Demo](https://codesandbox.io/s/vue-tf-worldmap-8xu3l?file=/src/App.vue)


## TODO

- Add testing
- Add documentation and usage examples
- Performance improvements
- Event handlers
- Styling optimization

## License

MIT