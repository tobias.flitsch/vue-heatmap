/**
 * Example usage of the vue-tf-worldmap component.
 * Copyright (C) 2021, Tobias Flitsch.
 */

import Vue from 'vue';
import WorldMap from '../src/main';
import App from './App.vue';

Vue.use(WorldMap);
Vue.config.productionTip = false;

// eslint-disable-next-line no-new
new Vue({
  el: '#app',
  components: { App },
  render: (h) => h(App),
});
